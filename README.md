**==============================**
**FREE FOR PERSONAL & COMMERCIAL**
**==============================**

This work is licensed under the Creative Commons Atribution 4.0 International by Hervy Qurrotul Ainur - [https://hervyqa.com](https://hervyqa.com).
To view a copy of this license, visit [https://creativecommons.org/licenses/by-/4.0/](https://creativecommons.org/licenses/by-/4.0/).

--------------------------------------------------
Visit other freebies: [https://hervyqa.com/freebies](https://hervyqa.com/freebies)
